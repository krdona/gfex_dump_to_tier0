# Various functions that we don't want cluttering up our notebook
import uuid

def RN():
    """
    Makes a random string name. Useful for naming ROOT objects, they want to have
    unique names but this can be a pain if you create these objects in loops in many places.
    It is more likely to be struck by lightning than to have this function give the same name
    twice in a session.
    """
    return str(uuid.uuid4())

def get_second_max(input_list):
    max_value = max(input_list)
    input_list.remove(max_value)
    second_max = max(input_list)
    return second_max

def get_max_and_index(input_list):
    max_value = max(input_list)
    index_of_max = input_list.index(max_value)
    return max_value, index_of_max