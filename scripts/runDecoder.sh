runnumber=00439798
luminumber=0702
foldername=run${runnumber}LB${luminumber}
cd BSDecodedData
mkdir $foldername
cd $foldername
for infile in $(ls /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_Main/${runnumber}/data22_13p6TeV.${runnumber}.physics_Main.daq.RAW/*lb${luminumber}*)
do
  python -m TrigT1ResultByteStream.TrigT1ResultByteStreamConfig --filesInput $infile --outputs gTOBs gCaloTowers --outputLevel DEBUG
done
cd ../../
