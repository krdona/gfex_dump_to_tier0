import argparse
import sys,os,glob,uuid
import datetime
import ROOT as root
import uproot as ur
import numpy as np
import matplotlib.pyplot as plt
sys.path.insert(1, 'gfex_compare_edits/')
import gfex_compare as comp
import csv

#argument parser for inputs on the command line
ap = argparse.ArgumentParser()

#create inputs
ap.add_argument("-r", "--runNumber", required=True, default=None, type=int, help="Run number of the event dump that was parsed")
#create inputs
ap.add_argument("-l", "--lumiBlockNumber", required=True, default=None, type=int, help="Lumiblock number of the event dump that was parsed")

#parse inputs
args = vars(ap.parse_args())

#assign inputs to variables
runNumber = args["runNumber"]
lumiNumber = str(args["lumiBlockNumber"]).zfill(4) #this pads the luminumber to be 4 digits
print(lumiNumber)

# Get the current working directory
cwd = os.getcwd()
mydirectory = cwd.replace("gFex_Dump_to_Tier0","")

input_TOBSfilePath = mydirectory + "/gfex_compare/ReadoutTOBfiles"

file_path_with_runnumber = input_TOBSfilePath + "/run" + str(runNumber) + "_dump_"
file_path_dict = {
    "ajtobs" : file_path_with_runnumber + "ajtobs.txt",
    "bjtobs" : file_path_with_runnumber + "bjtobs.txt",
    "cjtobs" : file_path_with_runnumber + "cjtobs.txt"
}

fpga_file = "ajtobs" # a random file for when we want to print things to make sure all of our expected keys are present

#create a list of the different types of files
# I am only investigating the "jtob" files right now, because the gtob files use a different function
# once I understand the differences between these files we can figure out what we need from both file types more carefully
fpga_file_names = list(file_path_dict.keys())

#create a dictonary that allows us to choose which fpga's file we care to investigate.
#the keys to this dictonary are the same as the keys for the file paths, defiend above
event_info_per_fpga_file = {}
bcids_dump = []

for f in fpga_file_names: #iterate through the different files
    lines = comp.get_lines(file_path_dict[f]) #def at line 49 of gfex_compare.py
    
    #using my own shortened version of anthony's function parse_jtobs in gfex_compare.py
    l1id, time_unix, bcid_hex = comp.parse_jtobs_for_eventinfo(lines)
    
    #using anthony's jTOB_full_parse function, I can extract all of the eta,phi, and energy information
    lead_g_l_phi, lead_g_l_eta, lead_g_l_en, lead_g_l_satbit, sub_g_l_phi, sub_g_l_eta, sub_g_l_en, sub_g_l_satbit, lead_j_l_phi, lead_j_l_eta, lead_j_l_en, lead_j_l_satbit, bcid_l, lead_g_r_phi, lead_g_r_eta, lead_g_r_en, lead_g_r_satbit, sub_g_r_phi, sub_g_r_eta, sub_g_r_en, sub_g_r_satbit, lead_j_r_phi, lead_j_r_eta, lead_j_r_en, lead_j_r_satbit, bcid_r = comp.jTOB_full_parse(lines, 10)
    
    #create an empty array that will store ALL of the event info for all of the events
    #this allows us to never have to open these files again
    event_info = []
    for event in range(len(l1id)): #iterate through every event, to then add all of them to the event_info array
        #create a dictonary for each individual event, that gives every variable its respective name
        #using the same variable names as Anthony defined in his gfex_compare.py script
        event_dictonary = { "l1id": int(l1id[event]),
                            "time_unix" : int(time_unix[event]),
                            "bcid" : int(bcid_hex[event], 16),
                            "lead_g_l_phi" : int(lead_g_l_phi[event]),
                            "lead_g_l_eta" : int(lead_g_l_eta[event]),
                            "lead_g_l_en" : int(lead_g_l_en[event]),
                            "sub_g_l_phi" : int(sub_g_l_phi[event]),
                            "sub_g_l_eta" : int(sub_g_l_eta[event]),
                            "sub_g_l_en" : int(sub_g_l_en[event]),
                            "lead_j_l_phi" : int(lead_j_l_phi[event]),
                            "lead_j_l_eta" : int(lead_j_l_eta[event]),
                            "lead_j_l_en" : int(lead_j_l_en[event]),
                            "lead_g_r_phi" : int(lead_g_r_phi[event]),
                            "lead_g_r_eta" : int(lead_g_r_eta[event]),
                            "lead_g_r_en" : lead_g_r_en[event],
                            "sub_g_r_phi" : int(sub_g_r_phi[event]),
                            "sub_g_r_eta" : int(sub_g_r_eta[event]),
                            "sub_g_r_en" : int(sub_g_r_en[event]),
                            "lead_j_r_phi" : int(lead_j_r_phi[event]),
                            "lead_j_r_eta" : int(lead_j_r_eta[event]),
                            "lead_j_r_en" : int(lead_j_r_en[event]),
                            "lead_j_r_satbit" : int(lead_j_r_satbit[event])
            }
        event_info.append(event_dictonary) #append this dictoanry to the array, so we have one for every event
    event_info_per_fpga_file[f] = event_info #append this array (for each event) to a dictornay that allows us to sort by fpga file

#### we have sorted the dumped information to be organized as is outputted by Anthony's parsing script
#this function organizes that information so that it matches the output format of the bytestream decoder
prop_key = { "Et" : "en", "Eta" : "eta", "Phi" : "phi" }
def fill_dumped_vals_like_AOD(size, phys_prop, d):
    dumped_format_like_AOD = [] #for each event create an array that will look like the AOD format array
    for fpga in fpga_file_names: #iterate through fpga a b and c
        if size == "SR": # small r gfex jets denoted with gjet, and we store the subleading info
            lead_l = event_info_per_fpga_file[fpga][d]["lead_g_l_"+prop_key[phys_prop]]
            sub_l = event_info_per_fpga_file[fpga][d]["sub_g_l_"+prop_key[phys_prop]]
            lead_r = event_info_per_fpga_file[fpga][d]["lead_g_r_"+prop_key[phys_prop]]
            sub_r = event_info_per_fpga_file[fpga][d]["sub_g_r_"+prop_key[phys_prop]]
            dumped_format_like_AOD.extend([lead_l, sub_l,lead_r, sub_r])
        if size == "LR": # large r gfex jets denoted with Jjet, and we store the subleading info
            lead_l = event_info_per_fpga_file[fpga][d]["lead_j_l_"+prop_key[phys_prop]]
            lead_r = event_info_per_fpga_file[fpga][d]["lead_j_r_"+prop_key[phys_prop]]
            dumped_format_like_AOD.extend([lead_l, lead_r])
    return dumped_format_like_AOD
#end of function
    
   
########################################
#accessing AOD saved to disck events that have been decoded
#######################################
filepath = mydirectory + "/BSDecodedData"
files = [f for f in glob.glob(filepath+"/run00"+str(runNumber)+"LB"+str(lumiNumber)+"/*.root")]
if len(files) < 1:
  print("oops you havent run the bytestream decoder over this run number and lumiblock combo yet")
  print("Pleas edit runDecoder.sh to use run " + str(runNumber) +" and lumiblock " + str(lumiNumber))
  print("Then you can run with `source runDecoder.sh`")
tree = []
for file in files:
    current_file = ur.open(file)
    keys = current_file.keys()
    for k in keys:
        if "CollectionTree" in k:
            tree.append(current_file[k])
            break

################
#saving events that seem to match between the dumped and aod formats to a csv
################
int_tree = 0 # a counter just as a sanity check to know how many files/trees we have opened
file_rows = []
bcids_aod = []
for t in tree: #iterates through all AOD trees from decoded data so one tree per file
    int_tree += 1
    print("____________________")
    print("tree " + str(int_tree) + " of " + str(len(tree)))
    L1ID_AOD = t["EventInfoAux.extendedLevel1ID"].array()
    BCID_AOD = t["EventInfoAux.bcid"].array()
    Time_AOD = t["EventInfoAux.timeStamp"].array()
    for a in range(len(L1ID_AOD)):
        AOD_L1ID = int(L1ID_AOD[a])
        AOD_BCID = int(BCID_AOD[a])
        AOD_time = int(Time_AOD[a])
        bcids_aod.append(AOD_BCID)
        
        for d in range(len(event_info_per_fpga_file[fpga_file])): #for each value in the dumped data
            Dump_L1ID = event_info_per_fpga_file[fpga_file][d]["l1id"]
            Dump_BCID = event_info_per_fpga_file[fpga_file][d]["bcid"]
            Dump_time = event_info_per_fpga_file[fpga_file][d]["time_unix"]

#            if (int(str(AOD_L1ID)[0:7]) == int(str(Dump_L1ID)[0:7])) and (AOD_BCID == Dump_BCID): #compare only the first 7 digits of the L1IDs
            if (AOD_L1ID == Dump_L1ID) and (AOD_BCID == Dump_BCID):
                print("Woohoo! I found one of the events I am looking for!")
                event_dictonary= { 'AOD_SRTobEt': t["L1_gFexSRJetRoIAux.gFexTobEt"].array()[a],
                                   'AOD_SRTobEta': t["L1_gFexSRJetRoIAux.iEta"].array()[a],
                                   'AOD_SRTobPhi': t["L1_gFexSRJetRoIAux.iPhi"].array()[a],
                                   'AOD_LRTobEt' : t["L1_gFexLRJetRoIAux.gFexTobEt"].array()[a],
                                   'AOD_LRTobEta' : t["L1_gFexLRJetRoIAux.iEta"].array()[a],
                                   'AOD_LRTobPhi' : t["L1_gFexLRJetRoIAux.iPhi"].array()[a],
                                   'AOD_L1ID' : AOD_L1ID,
                                   'AOD_BCID' : AOD_BCID,
                                   'AOD_time' : AOD_time,
                                   'Dump_SRTobEt': fill_dumped_vals_like_AOD("SR","Et", d),
                                   'Dump_SRTobEta': fill_dumped_vals_like_AOD("SR","Eta", d),
                                   'Dump_SRTobPhi': fill_dumped_vals_like_AOD("SR","Phi", d),
                                   'Dump_LRTobEt' : fill_dumped_vals_like_AOD("LR","Et", d),
                                   'Dump_LRTobEta' : fill_dumped_vals_like_AOD("LR","Eta", d),
                                   'Dump_LRTobPhi' : fill_dumped_vals_like_AOD("LR","Phi", d),
                                   'Dump_L1ID' : Dump_L1ID,
                                   'Dump_BCID' : Dump_BCID,
                                   'Dump_time' : Dump_time
                                 }
                file_rows.append(event_dictonary)
                header = list(event_dictonary.keys())


#header that describes the different variables we have for each event! These will be arrays in the CSV file
with open("CSVFiles_forMatchingEvents/AOD_and_dump_r"+str(runNumber)+"_l"+str(lumiNumber)+".csv", 'w', encoding='UTF8', newline='') as f:
    writer = csv.DictWriter(f, fieldnames=header)
    writer.writeheader()
    writer.writerows(file_rows)
    
#make a plot of all BCIDs in the AOD file
fig, ax = plt.subplots()
ax.hist(bcids_aod, bins=np.arange(min(bcids_aod), max(bcids_aod)))
plt.grid(True)
title = "AOD Data Distribution of BCID values written for \n lumiblock number " + str(lumiNumber) +  " in run " + str(runNumber)

plt.title(title, fontsize=15)
plt.xlabel("BCID Values", fontsize=15)
plt.ylabel("Count", fontsize=15)
fig.savefig("ComparisonPlots/" + str(runNumber) +"/BCID_Values_AOD_Tier0", bbox_inches='tight', dpi=300)
print("the minimum of bcid values in the offline Tier0 AOD file is " + str(min(bcids_aod)))
print("the maximum of bcid values in the offline Tier0 AOD file is " + str(max(bcids_aod)))
