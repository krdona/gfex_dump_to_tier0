# gfex_dump_to_tier0

  

 

  

## Getting started

  

Welcome to Kristin's respotory for finding data that the gfex team has dumped and accessing it in tier 0 to make some fun plots. Complete instructions on how to use this code, and the goal of it can be found in [this google slides deck](https://docs.google.com/presentation/d/1R1g_REI0GaGIdb_MjWt4xgFSVPUXZuW6ZznNjjA_Yy8/edit?usp=sharing)

  

### The recommended workflow is

1.  Find a set of dumped data you want to investigate

2.  Run FirstStep.sh to find the time stamp of the dumped data 
Go to run query to find what lumiblock the events took place in (probably just one, could spread between two)
    
3.  Go to [run query](https://atlas-runquery.cern.ch/query.py?q=find+run+439529+%2F+show+all) to find what lumiblock the events took place in (probably just one, could spread between two)
    
4.  Run SecondStep.sh to iterate through the available AOD offline files, and create a CSV file containing all of the matching events
    
5.  Run mergeCSV.py to merge all CSV files from multiple lumiblocks into one

6. Open KristingFexCompare.ipynb to generate plots that compare the events from both places


