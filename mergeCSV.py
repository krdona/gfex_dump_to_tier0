import argparse
import shutil
import glob
import os

#argument parser for inputs on the command line
ap = argparse.ArgumentParser()

#create inputs
ap.add_argument("-r", "--runNumber", required=False, default=None, type=int, help="Run number of the event dump that was parsed")
#parse inputs
args = vars(ap.parse_args())
#assign inputs to variables
runNumber = args["runNumber"]
path = "CSVFiles_forMatchingEvents"

if runNumber is None: allFiles = glob.glob(path + "/AOD_and_dump*.csv")
else: allFiles = glob.glob(path + "/AOD_and_dump_r" + str(runNumber) + "*.csv")
allFiles.sort()  # glob lacks reliable ordering, so impose your own if output order matters

if runNumber is None:
  isExist = os.path.exists(path+'/AllDataTogether')
  if not isExist: os. makedirs(path+'/AllDataTogether')
  with open(path+'/AllDataTogether/all.csv', 'wb') as outfile:
    for i, fname in enumerate(allFiles):
        with open(fname, 'rb') as infile:
            if i != 0:
                infile.readline()  # Throw away header on all but first file
            # Block copy rest of file from input to output without parsing
            shutil.copyfileobj(infile, outfile)
            print(fname + " has been imported.")
else:
  isExist = os.path.exists(path+'/CombineRun'+ str(runNumber))
  if not isExist:os. makedirs(path+'/CombineRun'+ str(runNumber))
  with open(path+'/CombineRun'+ str(runNumber) +'/AOD_and_dump_r'+ str(runNumber) +'_all.csv', 'wb') as outfile:
      for i, fname in enumerate(allFiles):
          with open(fname, 'rb') as infile:
              if i != 0:
                  infile.readline()  # Throw away header on all but first file
              # Block copy rest of file from input to output without parsing
              shutil.copyfileobj(infile, outfile)
              print(fname + " has been imported.")
