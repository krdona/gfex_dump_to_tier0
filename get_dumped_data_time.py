import argparse
import sys,os,glob,uuid
import datetime
sys.path.insert(1, 'gfex_compare_edits/')
import gfex_compare as comp

#argument parser for inputs on the command line
ap = argparse.ArgumentParser()

#create inputs
ap.add_argument("-r", "--runNumber", required=True, default=None, type=int, help="Run number of the event dump that was parsed")
#parse inputs
args = vars(ap.parse_args())

#assign inputs to variables
runNumber = args["runNumber"]

# Get the current working directory
cwd = os.getcwd()
mydirectory = cwd.replace("/gFex_Dump_to_Tier0","")
print(mydirectory)

input_TOBSfilePath = mydirectory + "/gfex_compare/ReadoutTOBfiles"
file_path_with_runnumber = input_TOBSfilePath + "/run" + str(runNumber) + "_dump_"

fpga_file = file_path_with_runnumber + "ajtobs.txt"
#create a dictonary that allows us to choose which fpga's file we care to investigate.
#the keys to this dictonary are the same as the keys for the file paths, defiend above
event_info = {}

lines = comp.get_lines(fpga_file) #def at line 49 of gfex_compare.py

#using my own shortened version of anthony's function parse_jtobs in gfex_compare.py
l1id, time_unix, bcid_hex = comp.parse_jtobs_for_eventinfo(lines)

#using anthony's jTOB_full_parse function, I can extract all of the eta,phi, and energy information
lead_g_l_phi, lead_g_l_eta, lead_g_l_en, lead_g_l_satbit, sub_g_l_phi, sub_g_l_eta, sub_g_l_en, sub_g_l_satbit, lead_j_l_phi, lead_j_l_eta, lead_j_l_en, lead_j_l_satbit, bcid_l, lead_g_r_phi, lead_g_r_eta, lead_g_r_en, lead_g_r_satbit, sub_g_r_phi, sub_g_r_eta, sub_g_r_en, sub_g_r_satbit, lead_j_r_phi, lead_j_r_eta, lead_j_r_en, lead_j_r_satbit, bcid_r = comp.jTOB_full_parse(lines, 10)

#create an empty array to store the time information about an event
event_info_per_event = []
for event in range(len(l1id)): #iterate through every event, to then add all of them to the event_info array
    #create a dictonary for each individual event, that gives every variable its respective name
    event_dictonary = {}
    
    #add all of the interesting variables with their name to the dictonary
    event_dictonary["time"] = datetime.datetime.fromtimestamp(int(time_unix[event])).strftime('%Y-%m-%d %H:%M:%S')
    event_dictonary["time_unix"] = int(time_unix[event])
    event_info_per_event.append(event_dictonary) #append this dictoanry to the array, so we have one for every event
print("_________________________________________________________")
print("Thanks for checking the timestamps of these dumped files")
print("Youre looking at run number: " + str(runNumber))
print("First event took place at " + event_info_per_event[0]['time'])
print("Last event took place at " + event_info_per_event[-1]['time'])
print("Dont forget to go to run query https://atlas-runquery.cern.ch/query.py?q=find+run+" + str(runNumber) +"+%2F+show+all")
print("to find out what lumiblock these timestamps correspond to")
print("_________________________________________________________")
